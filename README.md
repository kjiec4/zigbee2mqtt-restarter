# Utility to restart zigbee2mqtt in case of a coordinator stick timeout

## Basic usage

1. Place `zigbee2mqtt-restarter.sh` and `zigbee2mqtt_restarter.awk` from the repository `src` folder to the `/opt/zigbee2mqtt-restarter` folder of a target system;
2. Set owner for the `/opt/zigbee2mqtt-restarter` and its content to the `root`;
3. Place a `zigbee2mqtt-restarter.service` from the repository `src` folder to the `/etc/systemd/system` folder of a target system;
4. Set owner for the `zigbee2mqtt-restarter.service` to the `root`;
5. Reload systemd services with:
    
    ```sh
    sudo systemctl daemon-reload
    ```

6. Enable and start zigbee2mqtt-restarter systemd service:

    ```sh
    sudo systemctl enable zigbee2mqtt-restarter.service
    ```

    ```sh
    sudo systemctl start zigbee2mqtt-restarter.service
    ```

7. Check status for the service:

    ```sh
    sudo systemctl status zigbee2mqtt-restarter.service
    ```

## Why does one need these scripts?

After every restart of my Raspberry Pi 3b+ zigbee2mqtt there are timeout messages in the zigbee2mqtt log containing:

```
SRSP - AF - dataRequest after 6000ms
```

After those messages appeared nothing works anymore before zigbee2mqtt restart.

Unfortunately, I haven’t overcome this issue, so I’ve created scripts from this repo for automatic restarting of the zigbee2mqtt service in case of the timeout issue occurrence.