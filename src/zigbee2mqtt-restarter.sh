#!/bin/sh

journalctl -u zigbee2mqtt.service --since now -xef | awk -W interactive -f zigbee2mqtt_restarter.awk
